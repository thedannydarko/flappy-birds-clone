package com.tehdannydarko;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Input
    implements KeyListener {

    private boolean spacePressed = false;
    private boolean spaceReleased = true;

    public boolean isSpacePressed() {
        final boolean s = this.spacePressed;
        this.spacePressed = false;
        return s;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_SPACE && this.spaceReleased) {
            this.spacePressed = true;
            this.spaceReleased = false;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_SPACE) {
            this.spaceReleased = true;
        }

    }
}
