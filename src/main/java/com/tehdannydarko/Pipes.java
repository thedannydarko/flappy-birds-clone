package com.tehdannydarko;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Random;

public class Pipes implements Updateable, Renderable {

    private int pipeWidth = 100;
    private int pipeHorizontalSpacing = 210;
    private int pipeVerticalSpacing = 180;

    private float xVel = -5.0f;
    private float x1;
    private float x2;
    private float x3;
    private float y1;
    private float y2;
    private float y3;

    // the pipe that is closest to the bird
    private int currentPipe;
    // array to hold the pipes' coordinates
    private float[][] pipeCoords = new float[3][2];

    private Random rand;

    public Pipes() {
        this.rand = new Random();

        resetPipes();
    }

    public void resetPipes() {
        this.currentPipe = 0;

        this.x1 = Game.WIDTH * 2;
        this.x2 = this.x1 + this.pipeWidth + this.pipeHorizontalSpacing;
        this.x3 = this.x2 + this.pipeWidth + this.pipeHorizontalSpacing;

        this.y1 = getRandomY();
        this.y2 = getRandomY();
        this.y3 = getRandomY();
    }

    private int getRandomY() {
        return this.rand.nextInt(
                (int) (Game.HEIGHT * 0.4f) + (Game.HEIGHT / 10));
    }

    @Override
    public void render(Graphics2D g, float interpolation) {
        g.setColor(Color.BLUE);

        // pipe 1
        g.fillRect(
                (int) (this.x1 +  (this.xVel * interpolation)),
                0,
                this.pipeWidth,
                (int) y1);
        g.fillRect(
                (int) (this.x1 + (this.xVel * interpolation)),
                (int) y1 + this.pipeVerticalSpacing,
                this.pipeWidth,
                Game.HEIGHT);

        // pipe 2
        g.fillRect(
                (int) (this.x2 +  (this.xVel * interpolation)),
                0,
                this.pipeWidth,
                (int) y2);
        g.fillRect(
                (int) (this.x2 + (this.xVel * interpolation)),
                (int) y2 + this.pipeVerticalSpacing,
                this.pipeWidth,
                Game.HEIGHT);

        // pipe 3
        g.fillRect(
                (int) (this.x3 +  (this.xVel * interpolation)),
                0,
                this.pipeWidth,
                (int) y3);
        g.fillRect(
                (int) (this.x3 + (this.xVel * interpolation)),
                (int) y3 + this.pipeVerticalSpacing,
                this.pipeWidth,
                Game.HEIGHT);
    }

    @Override
    public void update(Input input) {
        this.x1 += this.xVel;
        this.x2 += this.xVel;
        this.x3 += this.xVel;

        if(this.x1 + this.pipeWidth < 0) {
            this.x1 = Game.WIDTH;
            this.y1 = getRandomY();
            this.currentPipe = 1;
        }

        if(this.x2 + this.pipeWidth < 0) {
            this.x2 = Game.WIDTH;
            this.y2 = getRandomY();
            this.currentPipe = 2;
        }

        if(this.x3 + this.pipeWidth < 0) {
            this.x3 = Game.WIDTH;
            this.y3 = getRandomY();
            this.currentPipe = 0;
        }

        // update the pipe coordinates
        switch(currentPipe) {
            case 0:
                this.pipeCoords[0][0] = x1;
                this.pipeCoords[0][1] = y1;
                break;
            case 1:
                this.pipeCoords[1][0] = x2;
                this.pipeCoords[1][1] = y2;
                break;
            case 2:
                this.pipeCoords[2][0] = x3;
                this.pipeCoords[2][1] = y3;
        }
    }

    public float[] getCurrentPipe() {
        return this.pipeCoords[this.currentPipe];
    }

    public int getCurrentPipeId() {
        return this.currentPipe;
    }

    public int getPipeWidth() {
        return this.pipeWidth;
    }

    public int getPipeHorizontalSpacing() {
        return this.pipeHorizontalSpacing;
    }

    public int getPipeVerticalSpacing() {
        return this.pipeVerticalSpacing;
    }
}
