package com.tehdannydarko;

public interface Updateable {

    void update(Input input);
}
